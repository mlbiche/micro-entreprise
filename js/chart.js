function drawChart(Écritures) {
  const dataChart = Écritures.dataChart()
  const canvas = document.getElementById('trésorerieChart')

  new Chart.Line(canvas, {
    type: 'line',
    xLabel: 'Jours',
    yLabel: 'Euros',
    data: {
      labels: dataChart.dates,
      datasets: [
        {
          label: 'Solde bancaire',
          data: dataChart.soldes,
          borderColor: '#e299ab'
        },
        {
          label: "Chiffre d'affaires",
          data: dataChart.chiffresAffaires,
          borderColor: '#398eb6'
        },
        {
          label: "Limite de chiffre d'affaires",
          data: dataChart.limiteChiffreAffaires,
          borderColor: '#fd7882',
          fill: false,
          hidden: true
        }
      ]
    },
    options: {
      scales: {
        xAxes: [
          {
            gridLines: {
              display: false
            }
          }
        ],
        yAxes: [
          {
            ticks: {
              beginAtZero: true
            }
          }
        ]
      }
    }
  })
}

export default drawChart
