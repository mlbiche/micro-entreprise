import WriteHTML from './writeHTML.js'

function build(Écritures, moisEnCours) {
  const écritures = Écritures.data
  // * présentation des soldes par mois
  panelSoldesEstimé(écritures)

  panelFactures({
    type: 'réglées',
    ...Écritures.facturesRéglées()
  })

  panelFactures({
    type: 'en-cours',
    ...Écritures.facturesEnCours()
  })

  panelFactures({
    type: 'prévues',
    ...Écritures.facturesPrévues()
  })

  panelDépenses({
    type: 'fixes',
    ...Écritures.dépensesFixes(moisEnCours)
  })

  panelTjm(Écritures.joursDeTravailRestants())

  function panelSoldesEstimé(écritures) {
    const solde = {
      banque: Écritures.solde(écritures, 'solde-bancaire'),
      chiffreAffaires: Écritures.solde(écritures, 'chiffre-affaires'),
      detteUrssaf: Écritures.solde(écritures, 'urssaf'),
      detteImpotSurRevenu:
        Écritures.solde(écritures, 'impot-sur-revenu-n') +
        Écritures.solde(écritures, 'impot-sur-revenu-n-1'),
      detteTva: Écritures.solde(écritures, 'tva')
    }
    solde.estimé =
      solde.banque +
      solde.detteUrssaf +
      solde.detteImpotSurRevenu +
      solde.detteTva
    WriteHTML.panelSoldesEstimé(solde)
  }

  function panelFactures({ type, solde, soldeHt, écritures, urssaf, tva }) {
    const soldes = {
      total: solde,
      CA: soldeHt,
      urssaf,
      tva
    }
    const écrituresClassées = écritures.sort((a, b) => a.date.isAfter(b.date))
    WriteHTML.panelFactures(type, soldes, écrituresClassées)
  }

  function panelDépenses({ type, solde, écritures }) {
    WriteHTML.panelDépenses(type, solde, écritures)
  }

  function panelTjm({ tjm, minimum, objectif, maximum }) {
    WriteHTML.panelTjm(tjm, { minimum, objectif, maximum })
  }
}

export default build
