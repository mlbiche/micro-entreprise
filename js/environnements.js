let écrituresFilename = './écritures.yml'
let dateDuJour = dayjs()
// environnement de test
// if (document.location.port === '8080') {
//   écrituresFilename = './test/écritures-fixtures.yml'
//   dateDuJour = dayjs(new Date(2022, 5, 10))
// }

export { écrituresFilename, dateDuJour }
