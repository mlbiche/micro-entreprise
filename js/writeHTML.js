import Bouture from 'https://cdn.jsdelivr.net/gh/DavidBruant/bouture@13cb6c683fa87e5feea574311dcda6353489bb3b/bouture.js'

function $name(name) {
  const html = document.querySelector(`[name=${name}]`)
  if (html) {
    return html
  } else {
    throw `le selecteur [name=${name}] ne retourne rien`
  }
}

const Write = {
  _solde: (selector, montant) => $name(selector).append(Math.round(montant)),

  _listePostes: (selector, postes) => {
    $name(selector).append(
      Bouture.tbody(
        postes.map(poste =>
          Bouture.tr([
            Bouture.td(poste.sousCatégorie),
            Bouture.td(Math.round(poste.montant))
          ])
        )
      ).getElement()
    )
  },

  _listeÉcritures: (selector, éléments) => {
    const thCollection = $name(selector).querySelector('thead > tr').children
    const libellés = Array.from(thCollection).map(th => th.textContent)
    $name(selector).append(
      Bouture.tbody(
        éléments.map(élément =>
          Bouture.tr(
            libellés.map(libellé => {
              const value = élément[libellé]
              if (libellé === 'montant') {
                return Bouture.td(Math.round(value))
              } else {
                if (value instanceof Date) {
                  return Bouture.td(dayjs(value).format('DD/MM/YYYY'))
                } else {
                  return Bouture.td(value)
                }
              }
            })
          )
        )
      ).getElement()
    )
  },

  panelSoldesEstimé: ({
    estimé,
    banque,
    chiffreAffaires,
    detteUrssaf,
    detteImpotSurRevenu,
    detteTva
  }) => {
    Write._solde('solde-estimé', estimé)
    Write._solde('solde-banque', banque)
    Write._solde('solde-chiffre-affaires', chiffreAffaires)
    Write._solde('solde-dette-urssaf', detteUrssaf)
    Write._solde('solde-dette-impot-sur-revenu', detteImpotSurRevenu)
    Write._solde('solde-dette-tva', detteTva)
  },

  panelFactures: (type, solde, écritures) => {
    Write._solde(`factures-${type}-solde`, solde.total)
    Write._solde(`factures-${type}-tva`, -solde.tva)
    Write._solde(`factures-${type}-urssaf`, -solde.urssaf)
    Write._listeÉcritures(`factures-${type}-liste`, écritures)
    if (type === 'réglées') {
      Write._solde(`factures-${type}-chiffre-affaires`, solde.CA)
    }
  },

  panelDépenses: (type, solde, postes) => {
    Write._solde(`dépenses-${type}-solde`, solde)
    Write._listePostes(`dépenses-${type}-liste`, postes)
  },

  panelTjm: (tjm, joursRestants) => {
    Write._solde('tjm', tjm)
    Object.entries(joursRestants).map(
      ([seuil, [seuilChiffreAffaires, joursTravailRestants]]) => {
        Write._solde(`seuil-ca-${seuil}`, seuilChiffreAffaires)
        Write._solde(`jours-travail-restant-${seuil}`, joursTravailRestants)
      }
    )
  }
}

export default Write
