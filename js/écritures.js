import Dates from './dates.js'

function estEntre(date, dateDébut, dateFin) {
  return !date.isBefore(dateDébut) && !date.isAfter(dateFin)
}

const CHIFFRE_AFFAIRES_MAXIMUM = 72600
const TAUX_COTISATIONS_SOCIALES = 0.211 + 0.002
const TAUX_TVA = 0.2

class Écritures {
  constructor(date) {
    // Define a range of one semester.
    this.jourStart = date
    this.jourEnd = date.add(6, 'month')
    this.data = []
  }

  async fetch(écrituresFilename) {
    let response
    try {
      response = await fetch(écrituresFilename)
      response = await response.text()
    } catch (error) {
      throw `écritures loading error: ${error}`
    }

    let écritures
    try {
      écritures = jsyaml.safeLoad(response)
    } catch (error) {
      throw `yaml to json error: ${error}`
    }
    this.data = this.avecDateEtChargesFixes(écritures)
  }

  extractSortedISODates() {
    return [
      ...new Set(
        this.data
          .filter(écriture =>
            estEntre(écriture.date, this.jourStart, this.jourEnd)
          )
          .map(écriture => écriture.date.format('YYYY-MM-DD'))
      )
    ].sort()
  }

  dataChart() {
    // La première valeur est fausse, alors on ne la passe pas au graphe.
    const sortedDates = this.extractSortedISODates()
    const soldeBancaire = this.soldesBancaire(sortedDates)
    const chiffresAffaires = this.chiffresAffaires(sortedDates)
    const limiteChiffreAffaires = this.limiteChiffreAffaires(sortedDates)
    return {
      dates: sortedDates,
      soldes: soldeBancaire.map(écriture => parseInt(écriture.solde)),
      chiffresAffaires,
      limiteChiffreAffaires
    }
  }

  soldesBancaire(dates) {
    return dates.map(date => this.soldeBancaire(dayjs(date), this.jourStart))
  }

  soldeBancaire(date) {
    const point = {}
    const { solde } = this.data
      .filter(écriture =>
        estEntre(écriture.date, this.jourStart, date.add(1, 'day'))
      )
      .reduce(
        (acc, cur) => {
          switch (cur.catégorie) {
            case 'factures':
              const urssaf = cur.montantHt * TAUX_COTISATIONS_SOCIALES
              const futurChiffreAffaires = acc.chiffreAffaires + cur.montantHt
              const montantHtSansUrssaf = cur.montantHt - urssaf
              acc.solde += montantHtSansUrssaf
              acc.chiffreAffaires = futurChiffreAffaires
              return acc

            case 'chargesFixes':
            case 'dettes':
              acc.solde += cur.montant
              return acc

            case 'divers':
              if (cur.sousCatégorie === 'solde-bancaire')
                acc.solde += cur.montant
              return acc

            default:
              return acc
          }
        },
        { solde: 0, chiffreAffaires: this.solde(this.data, 'chiffre-affaires') }
      )
    point.solde = solde

    return point
  }

  chiffresAffaires(dates) {
    return dates.map(date => this.chiffreAffaires(dayjs(date), this.jourStart))
  }

  chiffreAffaires(date) {
    return this.data
      .filter(écriture =>
        estEntre(écriture.date, this.jourStart, date.add(1, 'day'))
      )
      .reduce((acc, cur) => {
        switch (cur.catégorie) {
          case 'factures':
            return acc + cur.montantHt

          case 'divers':
            if (cur.sousCatégorie === 'chiffre-affaires')
              return acc + cur.montant
            return acc

          default:
            return acc
        }
      }, 0)
  }

  limiteChiffreAffaires(dates) {
    return dates.map(() => CHIFFRE_AFFAIRES_MAXIMUM)
  }

  ajouteSolde(écritures) {
    return {
      solde: écritures.reduce((acc, cur) => acc + cur.montant, 0),
      soldeHt: écritures.reduce((acc, cur) => acc + cur.montantHt, 0),
      écritures: écritures
    }
  }

  ajouteUrssaf(écrituresAvecSolde) {
    return {
      ...écrituresAvecSolde,
      urssaf: écrituresAvecSolde.soldeHt * TAUX_COTISATIONS_SOCIALES
    }
  }

  ajouteTva(écrituresAvecSoldeUrssaf) {
    return {
      ...écrituresAvecSoldeUrssaf,
      tva: (écrituresAvecSoldeUrssaf.solde * TAUX_TVA) / (1 + TAUX_TVA)
    }
  }

  dépensesFixes(month) {
    return this.ajouteSolde(
      this.data.filter(
        écriture =>
          écriture.date.month() + 1 === month &&
          écriture.catégorie === 'chargesFixes'
      )
    )
  }

  facturesRéglées() {
    return this.ajouteTva(
      this.ajouteUrssaf(
        this.ajouteSolde(
          this.data.filter(
            écriture =>
              écriture.catégorie === 'factures' &&
              écriture.sousCatégorie === 'dev' &&
              écriture.réglée
          )
        )
      )
    )
  }

  facturesEnCours() {
    return this.ajouteTva(
      this.ajouteUrssaf(
        this.ajouteSolde(
          this.data.filter(
            écriture =>
              écriture.catégorie === 'factures' &&
              écriture.sousCatégorie === 'dev' &&
              écriture.émise
          )
        )
      )
    )
  }

  facturesPrévues() {
    return this.ajouteTva(
      this.ajouteUrssaf(
        this.ajouteSolde(
          this.data.filter(
            écriture =>
              écriture.catégorie === 'factures' &&
              écriture.sousCatégorie === 'dev' &&
              écriture.prévue
          )
        )
      )
    )
  }

  joursDeTravailRestants() {
    function calculJoursDeTravailRestants(
      tjm,
      seuil,
      futurChiffreAffaires,
      isMax = false
    ) {
      const difference = seuil - futurChiffreAffaires
      if (!isMax && difference < 0) return 0
      return Math.round(difference / tjm)
    }

    const CHIFFRE_AFFAIRES_MINIMUM = 36000
    const CHIFFRE_AFFAIRES_OBJECTIF = 40000
    const tjmActuel = this.solde(this.data, 'tjm')
    const chiffreAffairesActuel = this.solde(this.data, 'chiffre-affaires')
    const facturesEnCours = this.facturesEnCours()
    const facturesPrévues = this.facturesPrévues()
    const futurChiffreAffaires =
      chiffreAffairesActuel + facturesEnCours.soldeHt + facturesPrévues.soldeHt
    const joursRestantsMinimum = calculJoursDeTravailRestants(
      tjmActuel,
      CHIFFRE_AFFAIRES_MINIMUM,
      futurChiffreAffaires
    )
    const joursRestantsObjectif = calculJoursDeTravailRestants(
      tjmActuel,
      CHIFFRE_AFFAIRES_OBJECTIF,
      futurChiffreAffaires
    )
    const joursRestantsMaximum = calculJoursDeTravailRestants(
      tjmActuel,
      CHIFFRE_AFFAIRES_MAXIMUM,
      futurChiffreAffaires,
      true
    )

    return {
      tjm: tjmActuel,
      minimum: [CHIFFRE_AFFAIRES_MINIMUM, joursRestantsMinimum],
      objectif: [CHIFFRE_AFFAIRES_OBJECTIF, joursRestantsObjectif],
      maximum: [CHIFFRE_AFFAIRES_MAXIMUM, joursRestantsMaximum]
    }
  }

  solde(écritures, sousCatégorie) {
    return écritures.reduce((acc, cur) => {
      if (cur.sousCatégorie === sousCatégorie) return acc + cur.montant
      return acc
    }, 0)
  }

  avecDateEtChargesFixes(écritures) {
    return this.avecChargesFixes(
      this.avecDescriptionDateEtMontantHt(
        this.avecCatégoriesEtSousCatégories(écritures)
      )
    )
  }

  /**
   * Récupère un tableau avec les écritures
   * en intégrant la catégorie et le sous-catégorie
   *
   *
   * @param {écritures} object
   *  écritures récupérées du fichier Yaml.
   * @return array
   *  écritures intégrant la catégorie et la sous-catégorie
   */
  avecCatégoriesEtSousCatégories(écritures) {
    return Object.entries(écritures)
      .map(([catégorie, sousCatégories]) =>
        Object.entries(sousCatégories).map(
          ([sousCatégorie, écrituresDeLaSousCatégorie]) =>
            (écrituresDeLaSousCatégorie || []).map(écriture => ({
              ...écriture,
              catégorie: catégorie,
              sousCatégorie: sousCatégorie
            }))
        )
      )
      .flat()
      .flat()
  }

  /**
   * Récupère un tableau avec les écritures
   * en intégrant la description et la date
   *
   *
   * @param {écritures} array
   *  écritures avec catégories et sous-catégories
   * @return array
   *  écritures la description et la date
   */
  avecDescriptionDateEtMontantHt(écritures) {
    return écritures.map(écriture => {
      const description = `${écriture.projet} - ${écriture.montant}`
      let montantHt = écriture.montant
      if (écriture.catégorie === 'factures')
        montantHt = écriture.montant / (1 + TAUX_TVA)
      let date
      if (écriture.prévue) {
        if (écriture.pièce) {
          throw `Erreur: facture prévue avec pièce: ${description}`
        }
        date = dayjs(écriture.prévue)
      }
      if (écriture.émise) {
        if (!écriture.pièce) {
          throw `Erreur: facture émise sans pièce: ${description}`
        }
        date = dayjs(écriture.émise).add(1, 'month')
      }
      if (écriture.réglée) {
        if (!écriture.pièce) {
          throw `Erreur: facture réglée sans pièce: ${description}`
        }
        date = dayjs(écriture.réglée)
      }
      if (écriture['jour-de-prélèvement']) {
        date = this.jourStart.date(écriture['jour-de-prélèvement'])
        // FIXME : vérifier comment est prélevée la tva mensuelle
        if (écriture.catégorie === 'dettes' && date.isBefore(this.jourStart))
          date = date.add(1, 'month')
      }
      if (!date) {
        date = this.jourStart
      }

      return { ...écriture, description, date, montantHt }
    })
  }

  /*
   * Ajoute les charges fixes du prochain mois jusqu'au dernier mois dans le semestre
   */
  avecChargesFixes(écritures) {
    const écrituresAvecChargesFixes = [...écritures]
    const chargesFixes = écrituresAvecChargesFixes.filter(
      écriture => écriture.catégorie === 'chargesFixes'
    )
    const nextMonth = this.jourStart.add(1, 'month')
    const annéesMoisÀCharger = Dates.prochainSemestre(nextMonth, this.jourEnd)

    annéesMoisÀCharger.forEach(([année, mois]) => {
      chargesFixes.forEach(chargeFixe => {
        écrituresAvecChargesFixes.push({
          ...chargeFixe,
          date: dayjs([année, mois, chargeFixe['jour-de-prélèvement']])
        })
      })
    })

    return écrituresAvecChargesFixes
  }
}

export default Écritures
