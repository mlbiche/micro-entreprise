/**
 * Calcule toutes les dates mensuelles au sein du prochain semestre
 *
 * @param {jourStart} jourStart Le début du semestre
 * @param {jourEnd} jourEnd La fin du semestre
 * @returns Un tableau de couple année/mois du prochain semestre
 */
function prochainSemestre(jourStart, jourEnd) {
  const annéeMois = []
  let currentDate = jourStart

  while (currentDate.isBefore(jourEnd) && annéeMois.length < 6) {
    annéeMois.push([currentDate.year(), currentDate.month() + 1])
    currentDate = currentDate.add(1, 'month')
  }
  return annéeMois
}

export default {
  prochainSemestre
}
