# pourquoi simulation ?

Pour voir où la trésorerie de ma micro-entreprise m'amène

Le solde bancaire ne reflète pas ma situation réelle, la simulation permet de se rendre compte de ma situation à jour et pour les mois à venir

## notice pour lire les tableaux

### conventions

Les prix sont exprimés en euros et Toutes Taxes Comprises, sauf le Taux Journalier Moyen

### charges fixes

Le tableau regroupe les charges mensuelles

Les charges annuelles on été ramenées au mois

### les factures

Liste des factures que j'adresse aux partenaires (clients)

#### réglées

les factures éditées et réglées

#### en cours

les factures éditées et non-réglées

#### à venir

les factures que nous allons éditer bientôt

## comment actualiser les écritures

à documenter

### grammaire des fichiers yaml

à documenter

### glossaire

- **écriture** : transcription d'un flux financier pour un compte
- **poste** : un poste est un regroupement de comptes - par exemple, le compte -salaires- regroupe les écritures de salaires des quatre salariés
- **charges fixes** : dépenses mensuelles récurrentes qui définissent le seuil de rentabilité
- **solde** : le solde des comptes représentent le total des montants
- **pièces** : justificatif comptable : facture client ou facture dépense

## outils utilisés pour le projet

- https://github.com/redhat-developer/yaml-language-server
- https://prettier.io

## remerciements

- @thimy https://github.com/etalab/template.data.gouv.fr
- @davidbruant https://github.com/davidbruant/bouture
- Scopyleft https://gitlab.com/scopyleft/simulation

## Installation en local

```bash
$ npm install --only=prod
$ ln -s node_modules modules
$ python3 -m http.server 8000 --bind 127.0.0.1
```

Puis : http://127.0.0.1:8000

## développement

```bash
$ npm install
$ ln -s node_modules modules
$ http-server
```

Puis : http://127.0.0.1:8080

Le _port 8080_ est identifié comme environnement de test
Les _[fixtures](https://gitlab.com/scopyleft/simulation/blob/master/test/%C3%A9critures-fixtures.yml)_ (données de test) sont chargées à la place des [écritures réelles](https://gitlab.com/scopyleft/simulation/blob/master/%C3%A9critures.yml)

## les tests

```bash
$ npm run test
```
